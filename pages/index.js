import Header from "../components/Header";
import '../scss/style.scss';

const Index = () => (
	<section>
		<Header />
		<h1>Hello, this is an example of Next JS</h1>
	</section>
);

export default Index;
