const express = require('express');
const next = require('next');
const routes = require('./routes');
const port = process.env.PORT || 3000;
const app = next({ dev: process.env.NODE_ENV !== 'production' });
const handler = app.getRequestHandler(app);

app
	.prepare()
	.then(() => {
		const server = express();

		server.get('*', (req, res) => {
			return handle(req, res);
		});

		server.use(handler).listen(port, (err) => {
			if (err) throw err;
			console.log(`> Ready on ${port}`);
		});
	})
	.catch((ex) => {
		console.error(ex.stack);

		process.exit(1);
	});
